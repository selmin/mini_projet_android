package com.example.miniprojet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public TextView username;
    public TextView password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.username =(TextView) findViewById(R.id.username);
        this.password =(TextView) findViewById(R.id.password);
    }
    public void logIn(View v){
        if(username.getText().toString().equals("admin") && password.getText().toString().equals("admin")){
            Intent i = new Intent(MainActivity.this, AdminInterface.class);
            startActivity(i);
        }else if(username.getText().toString().equals("membre") && password.getText().toString().equals("membre")){
            Intent i = new Intent(MainActivity.this, MemberInterface.class);
            startActivity(i);
        }
        else{
            //incorrect
            Toast.makeText(MainActivity.this,"LOGIN FAILED !!!",Toast.LENGTH_SHORT).show();
        }
    }
    public void register(View v){
        Intent i = new Intent(MainActivity.this, RegisterInterface.class);
        startActivity(i);
    }
}