package com.example.miniprojet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class AdminInterface extends AppCompatActivity {
    public ListView lst;
    ArrayList<String> allMembers;
    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_interface);
        this.lst = (ListView) findViewById(R.id.list);
        databaseReference = FirebaseDatabase.getInstance().getReference("Members");

    }
    public void getList(View v){
        getMembersName();
        ArrayAdapter adapter = new ArrayAdapter<String>(AdminInterface.this,
                android.R.layout.simple_dropdown_item_1line,this.allMembers);
        lst.setAdapter(adapter);
    }
    public void getMembersName(){
        this.allMembers.clear();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot ds : snapshot.getChildren()){
                    Members m = ds.getValue(Members.class);
                    allMembers.add(m.getName());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(AdminInterface.this, "Failed to get data", Toast.LENGTH_SHORT).show();
            }
        });
    }
}