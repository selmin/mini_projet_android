package com.example.miniprojet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterInterface extends AppCompatActivity {
    public EditText editName;
    public EditText editUserName;
    public EditText editPassword;
    DatabaseReference ref;
    Members member;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_interface);
        this.editName = this.findViewById(R.id.name);
        this.editUserName = this.findViewById(R.id.userName);
        this.editPassword = this.findViewById(R.id.password);
        member = new Members();
        ref = FirebaseDatabase.getInstance().getReference();
    }

    public void CommitData(View v){
        String name = editName.getText().toString();
        String userName = editUserName.getText().toString();
        String password = editPassword.getText().toString();
        member.setName(name);
        member.setUserName(userName);
        member.setPassword(password);


        ref.child(userName).setValue(member);
        Intent i = new Intent(RegisterInterface.this, MainActivity.class);
        startActivity(i);
    }
}